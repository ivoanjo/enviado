lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'enviado/version'

Gem::Specification.new do |spec|
  spec.name          = 'enviado'
  spec.version       = Enviado::VERSION
  spec.authors       = ['Ivo Anjo']
  spec.email         = ['ivo.anjo@ist.utl.pt']
  spec.license       = 'LGPL-3.0+'

  spec.summary       = %q{A ruby wrapper for the envoy proxy}
  spec.homepage      = 'https://gitlab.com/ivoanjo/enviado'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 11.3'
  spec.add_development_dependency 'rspec', '~> 3.5'
  spec.add_development_dependency 'pry'
end
