require 'faraday'
require 'pry'
require 'rack'
require 'enviado'

REQUESTS=100
PORT=8080

def start_web_server
  Thread.new do
    Rack::Handler::WEBrick.run(
      proc { |env|
        if rand <= 0.5
          [200, {"Content-Type" => "text/html"}, ["Hello, World!\n"]]
        else
          [500, {"Content-Type" => "text/html"}, ["I am a small potato\n"]]
        end
      },
      Host: '127.0.0.1',
      Port: PORT,
      Logger: WEBrick::Log.new('/dev/null'),
      AccessLog: [],
    )
  end

  sleep 1
end

def perform_requests(url)
  connection = Faraday.new(url: url)

  codes = Hash.new { |h, k| h[k] = 0 }

  REQUESTS.times do
    response = connection.get
    codes[response.status] += 1
  end

  codes
end

def main
  puts "Starting web server..."
  start_web_server

  puts "Doing #{REQUESTS} requests WITHOUT enviado..."
  puts "Results: #{perform_requests('http://localhost:8080')}"

  puts "Starting enviado"
  Enviado.start(config_path: 'envoy_service_to_service_simple.json')

  puts "Doing #{REQUESTS} requests WITH enviado..."
  puts "Results: #{perform_requests('http://localhost:9211')}"
end

main
