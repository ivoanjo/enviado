# Enviado: A ruby wrapper for the envoy proxy
# Copyright (C) 2016 Ivo Anjo <ivo.anjo@ist.utl.pt>
#
# This file is part of Enviado.
#
# Enviado is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Enviado is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Enviado.  If not, see <http://www.gnu.org/licenses/>.

module Enviado
  VERSION = '0.0.3.beta1'
end
