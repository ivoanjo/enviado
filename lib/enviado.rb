# Enviado: A ruby wrapper for the envoy proxy
# Copyright (C) 2016 Ivo Anjo <ivo.anjo@ist.utl.pt>
#
# This file is part of Enviado.
#
# Enviado is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Enviado is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Enviado.  If not, see <http://www.gnu.org/licenses/>.

require 'enviado/version'

module Enviado
  ENVOY_PATH = File.expand_path("../ext/#{RUBY_PLATFORM}/envoy", __FILE__)

  class << self
    def start(config_path:)
      pid = spawn("#{ENVOY_PATH} -c #{config_path}")
      sleep 5 # for good measure! ;)
      pid
    end
  end
end
