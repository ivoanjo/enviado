# Enviado

Enviado is a ruby wrapper for the [envoy](https://github.com/lyft/envoy) proxy, making it easy to use enviado with your services or gems in a transparent way.

Because enviado embeds a binary version of `envoy`, it needs a specific binary for each operating system. Right now it only supports `x86_64-linux-gnu` but PRs are welcome to add more binaries!

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'enviado'
```

And then execute:

```bash
$ bundle
```

Or install it yourself as:

```bash
$ gem install enviado
```

## Usage

To start envoy with a given [configuration file](https://lyft.github.io/envoy/docs/configuration/configuration.html):

```ruby
require 'enviado'

Enviado.start(config_path: 'path/to/envoy/config/file.json')
```

Now you can perform requests to envoy.

To see an example of an envoy configuration that deals with a flaky service, see [examples/flaky_requests/flaky_requests.rb](examples/flaky_requests/flaky_requests.rb).

Running in docker:

```bash
$ cd examples/flaky_requests.rb
$ docker-compose build example && docker-compose run example
Starting web server...
Doing 100 requests WITHOUT enviado...
Results: {500=>44, 200=>56}
Starting enviado
[2016-11-19 13:14:58.039][211][warning][main] initializing epoch 0 (hot restart version=3.2490504)
[2016-11-19 13:14:58.041][211][warning][main] all clusters initialized. starting workers
[2016-11-19 13:14:58.041][211][warning][main] starting main dispatch loop
Doing 100 requests WITH enviado...
Results: {200=>100}
```

## Development

After checking out the repo, run `bundle install` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at <https://gitlab.com/ivoanjo/enviado>.
